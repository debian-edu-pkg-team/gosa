GOsa 2.8 for Debian
-------------------

* Configure GOsa

By default you can point your favorite browser to the GOsa setup by
using this URL:

http://you.server.address/gosa

Follow the instructions on the screen.


* Security related information

GOsa is running as the www-data user. This makes it possible for other
web applications (well, this is the rule for almost every web application
that stores information somewhere around) to read the gosa.conf file, which
may contain vital information about your LDAP service.

To make it harder to extract these passwords, they get encrypted by a
master password only readable by the GOsa location.

You can simply migrate old existing passwords by typing:

# a2enmod headers
# gosa-encrypt-passwords
# /etc/init.d/apache2 reload

If this is not enough for you (exploitable PHP code may make it possible to
read the webservers memory), you can simply create another webserver instance
running as a different user on different port for GOsa exclusively. Or use
apache2-mpm-itk and assign a different user to a virtual host.


* Generic information

Getting GOsa running itself is not very complicated. Problems normally
arise when integrating it in various services.

To play nice with your LDAP, you need to include the gosa schema files
into your LDAP configuration.

For Debian, you should install the gosa-schema package and take a look at
the sample slapd.conf provided in /usr/share/doc/gosa/contrib/openldap.

The GOsa² schemas are located in /etc/ldap/schema/gosa.

Add these lines to slapd.conf for loading GOsa² schema files into slapd:

# These should be present for GOsa.
include         /etc/ldap/schema/gosa/samba3.schema
include         /etc/ldap/schema/gosa/gosystem.schema
include         /etc/ldap/schema/gosa/gofon.schema
include         /etc/ldap/schema/gosa/gofax.schema
include         /etc/ldap/schema/gosa/goto.schema
include         /etc/ldap/schema/gosa/goserver.schema
include         /etc/ldap/schema/gosa/gosa-samba3.schema
include         /etc/ldap/schema/gosa/trust.schema
