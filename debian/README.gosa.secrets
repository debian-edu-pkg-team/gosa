# GOsa² <-> LDAP with encrypted LDAP-Admin credential

## Pre-Amble

In 2018, the GOsa² internal encryption mechanism for the LDAP Admin's DN
master password has been ported from php-mcrypt to openSSL. This requires
the site admin to re-create the encrypted password in
/etc/gosa/gosa.secrets.

Below you find a howto provided by Benjamin Zapiec, originally posted at
https://github.com/gosa-project/gosa-core/issues/13

This howto applies if the Debian gosa package has been upgraded from a
version below 2.7.4+reloaded3-1. Furthermore, people identified problems
until 2.7.4+reloaded3-3, that finally got fixed in 2.7.4+reloaded3-4. If
you run any of the above versions, make sure you read the following
documentation properly before reporting your issue. It is likely that
this HowTo soothes your woes.

## GOsa Encryption HowTo

To solve your problem make sure your credentials are correct and that
gosa is able to reach your LDAP.

Maybe try to use a ``/etc/gosa/gosa.conf`` with plain passwords first. If that
works you need to run the following steps to encrypt your passwords.

  1. make sure you have the latest version of the encryption script:
     a389ec1

  2. make sure there is no "gosa.secrets" - file in your ``/etc/gosa``

  3. run the encryption script as root: "gosa-encrypt-passwords"

At this point you should have a ``gosa.secrets`` in ``/etc/gosa/``. Its
content should look like this:

```
    RequestHeader set GOSAKEY YOUR_SECRET
```

The secret is auto-generated.

In most cases you don't need to care about permissions but your
Web-Server has to be able to read the file since it will be included.
So the script will set ``gosa.secrets`` to read only just for root.

Check that ``gosa.conf`` contains password hashes now.

  4. Configure you Web-Server. Example from the script:

```
    Alias /gosa /usr/share/gosa/html

    <Location /gosa>
    php_admin_flag engine on
    php_admin_value open_basedir "/etc/gosa/:/usr/share/gosa/:/var/cache/gosa/:/var/spool/gosa/"
    php_admin_flag register_globals off
    php_admin_flag allow_call_time_pass_reference off
    php_admin_flag expose_php off
    php_admin_flag zend.ze1_compatibility_mode off
    include /etc/gosa/gosa.secrets
```

This example is for an older version of the apache Web-Server.
The most important part is

```
    include /etc/gosa/gosa.secrets
```

This directive will include your gosa.secrets. This file is used to set
an additional "RequestHeader" containing your secret only visible for
backend processing.

This header must be available to the backend. Otherwise
encrypted passwords don't work.

If you don't use apache check the documentation of your web-server
how to set additional request header.

If you do use apache make sure your gosa configuration is used.
If you have "" conditions in your config it may happen
that the configuration isn't use. The

```
    Alias /gosa /usr/share/gosa/html
```

itself will enable apache to access gosa. Since there is an
index.html you will see the login page. But that doesn't mean
that the "<Location ..>" is used.

After all these steps you should be able to use encrypted
passwords.

Best regards
Benjamin Zapiec & Mike Gabriel
